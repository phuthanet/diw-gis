<?php

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;


return [
    'router' => [
        'routes' => [

            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'admin' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/admin[/:action]',
                    'defaults' => [
                        'controller' => Controller\AdminController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'api' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/api[/:action]',
                    'defaults' => [
                        'controller' => Controller\ApiController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'dashboard' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/dashboard[/:action]',
                    'defaults' => [
                        'controller' => Controller\DashboardController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'layer' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/layer[/:action]',
                    'defaults' => [
                        'controller' => Controller\LayerController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'login' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/login[/:action]',
                    'defaults' => [
                        'controller' => Controller\LoginController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'map' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/map[/:action]',
                    'defaults' => [
                        'controller' => Controller\MapController::class,
                        'action'     => '',
                        // 'action'     => 'index',
                    ],
                ],
            ],

            'profile' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/profile[/:action]',
                    'defaults' => [
                        'controller' => Controller\ProfileController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'report' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/report[/:action]',
                    'defaults' => [
                        'controller' => Controller\ReportController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'user' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/user[/:action]',
                    'defaults' => [
                        'controller' => Controller\UserController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'news' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/news[/:action]',
                    'defaults' => [
                        'controller' => Controller\NewsController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'about' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/about[/:action]',
                    'defaults' => [
                        'controller' => Controller\AboutController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'document' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/document[/:action]',
                    'defaults' => [
                        'controller' => Controller\DocumentController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'results' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/results[/:action]',
                    'defaults' => [
                        'controller' => Controller\ResultsController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'target' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/target[/:action]',
                    'defaults' => [
                        'controller' => Controller\TargetController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'nontarget' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/nontarget[/:action]',
                    'defaults' => [
                        'controller' => Controller\NontargetController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'factory' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/factory[/:action]',
                    'defaults' => [
                        'controller' => Controller\FactoryController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

        ],
    ],
    'controllers' => [
        'factories' => [
            //Controller\IndexController::class => InvokableFactory::class,
            Controller\AdminController::class => Factory\DefaultControllerFactory::class,
            Controller\ApiController::class => Factory\DefaultControllerFactory::class,
            Controller\DashboardController::class => Factory\DefaultControllerFactory::class,
            Controller\IndexController::class => Factory\DefaultControllerFactory::class,
            Controller\LayerController::class => Factory\DefaultControllerFactory::class,
            Controller\LoginController::class => Factory\DefaultControllerFactory::class,
            Controller\MapController::class => Factory\DefaultControllerFactory::class,
            Controller\ProfileController::class => Factory\DefaultControllerFactory::class,
            Controller\ReportController::class => Factory\DefaultControllerFactory::class,
            Controller\UserController::class => Factory\DefaultControllerFactory::class,
            Controller\NewsController::class => Factory\DefaultControllerFactory::class,
            Controller\AboutController::class => Factory\DefaultControllerFactory::class,
            Controller\DocumentController::class => Factory\DefaultControllerFactory::class,
            Controller\ResultsController::class => Factory\DefaultControllerFactory::class,
            Controller\TargetController::class => Factory\DefaultControllerFactory::class,
            Controller\NontargetController::class => Factory\DefaultControllerFactory::class,
            Controller\FactoryController::class => Factory\DefaultControllerFactory::class
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    // The following registers our custom view helper classes.
    'view_helpers' => [
        'factories' => [
            View\Helper\Common::class => InvokableFactory::class,
        ],
        'aliases' => [
            'common' => View\Helper\Common::class,
        ]
    ],
];
