<?php

namespace Application\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;

use Zend\Paginator\Paginator;


use Utils\Utils;

class Layer
{
    private $factory;
    private $adapter;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
        $this->adapter = $factory['adapter'];
    }

    public function paginator($search)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->from('layers')
            ->join('layers_group', 'layers.layer_group_id = layers_group.layer_group_id', ['layer_group_name'], 'LEFT')
            ->where('layer_active = 1')
            ->where("(layer_name LIKE '%" . $search . "%')")
            ->order(["layer_group_order ASC", "layer_order ASC", "layer_name DESC"]);

        // echo $db->buildSqlString($select); exit();

        $resultSetPrototype = new ResultSet();

        $paginatorAdapter = new DbSelect(
            $select,
            $this->adapter['read'],
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function lists($search)
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->from('layers')
            ->join('layers_group', 'layers.layer_group_id = layers_group.layer_group_id', ['layer_group_name'], 'LEFT')
            ->where('layer_active = 1')
            ->where("(layer_name LIKE '%" . $search . "%')")
            ->order(["layer_group_order ASC", "layer_order ASC", "layer_name DESC"]);
        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();
        return $results;
    }


    public function get($id = null, $type = null, $search = '')
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->from('layers')
            ->join('layers_group', 'layers.layer_group_id = layers_group.layer_group_id', ['layer_group_name'], 'LEFT')
            ->where(["layer_name LIKE '%" . $search . "%'"]);

        if (!is_null($id)) {
            $select->where(['layer_id' => $id]);
        }

        if (!is_null($type)) {
            $select->where(['layer_type' => $type]);
        }

        // echo $db->buildSqlString($select);
        // exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        if (!is_null($id) && !is_array($id)) {
            return $results->current();
        } else {
            return $results;
        }
    }

    public function getEditLayerInfo($layerId, $groupId)
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->from('edit_layer')
            ->where(['layer_id' => $layerId, 'user_group_id' => $groupId]);

        // echo $db->buildSqlString($select);
        // exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        return $results->current();
    }

    public function getLayerObjectIdByGroup($groupId)
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('edit_layer')
            ->where(['user_group_id' => $groupId]);


        // echo $db->buildSqlString($select);
        // exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        $objectId = $results->current();


        return $objectId['layer_object_id'];
    }

    public function saveAttribute($data, $id)
    {
        $table = new TableGateway('layers', $this->adapter['write']);

        $json = json_encode($data);
        $table->update(['layer_attribute' => $json], ['layer_id' => $id]);
    }


    public function getGroupLayer($id = null, $order = 'DESC')
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('layers_group')
            ->where('layer_group_active = 1')
            ->order(['layer_group_order ' . $order]);

        if (!is_null($id)) {
            $select->where(['layaer_group_id' => $id]);
        }

        // echo $db->buildSqlString($select);
        // exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        if (!is_null($id)) {
            return $results->current();
        } else {
            return $results;
        }
    }

    public function getLayersByGroup($id)
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('layers')
            ->where(['layer_group_id' => $id, 'layer_active' => 1])
            ->order(['layer_order DESC', 'layer_name ASC']);

        // echo $db->buildSqlString($select);
        // exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        return $results;
    }


    public function save($data, $id = null)
    {

        $table = new TableGateway('layers', $this->adapter['write']);

        if (is_null($id)) {
            //insert
            $data['layer_attribute'] = '{}';
            $table->insert($data);
            $id = $table->lastInsertValue;
        } else {
            //update
            $table->update($data, ['layer_id' => $id]);
        }
        return $id;
    }

    public function sort($id, $sort)
    {

        $db = new Sql($this->adapter['read']);

        $layer = $this->get($id);

        $select = $db->Select()
            ->from('layers')
            ->where(['layer_group_id' => $layer['layer_group_id']])
            ->order(['layer_order' => 'ASC']);

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        $layerList = [];
        $i = 0;
        $index = 0;
        foreach ($results as $row) {
            $layerList[$i] = $row;
            if ($row['layer_id'] == $id) {
                $index = $i;
                $oldOrder = $row['layer_order'];
            }
            $i++;
        }

        if ($sort == 'up') {
            $index--;
        } else {
            $index++;
        }

        if (isset($layerList[$index])) {
            $table = new TableGateway('layers', $this->adapter['write']);

            $newOrder = $layerList[$index]['layer_order'];
            if ($sort == 'up') {

                // echo 'new [' . $order . ']';
                // echo ', ';
                // echo 'old [' . $layerList[$index]['layer_order'] . ']';
                // exit();
                $table->update(['layer_order' => $oldOrder], ['layer_order' => $newOrder]);
                $table->update(['layer_order' => $newOrder], ['layer_id' => $id]);
            } else {
                $table->update(['layer_order' => $oldOrder], ['layer_order' => $newOrder]);
                $table->update(['layer_order' => $newOrder], ['layer_id' => $id]);
            }
        }
    }

    public function getLastOrder()
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->columns([new Expression('MAX(layer_order) AS layer_order')])
            ->from('layers');

        // echo $db->buildSqlString($select);
        // exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        $layer = $results->current();
        return $layer['layer_order'];
    }


    public function disable($id)
    {

        $this->save(['layer_active' => 0], ['layer_id' => $id]);
    }
    public function delete($id)
    {
        $table = new TableGateway('layers', $this->adapter['write']);
        $table->delete(['layer_id' => $id]);
    }
}
