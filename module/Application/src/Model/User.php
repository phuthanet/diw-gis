<?php

namespace Application\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;

use Zend\Paginator\Paginator;
use Application\Model\Group;

use Utils\Utils;

class User
{
    private $factory;
    private $adapter;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
        $this->adapter = $factory['adapter'];
    }


    public function login($login, $password)
    {
        $db = new Sql($this->adapter['read']);

        // $select = $db->select()
        //     ->from('user')
        //     ->where(['user_login' => $login, 'user_password' => md5($password)])
        //     ->where('user_active = 1');

        // if ($login != 'admin') {
        //     $select->join('user_group', 'user.user_group_id = user_group.user_group_id', ['user_group_name'], 'LEFT');
        // }

        // // echo $db->buildSqlString($select); exit();

        // $stmt = $db->prepareStatementForSqlObject($select);
        // $results = $stmt->execute();
        $file = "http://www3.diw.go.th/session/LoginAdmin.asp?Login=" . $login . "&Passwd=" . $password . "&IP=" . $_SERVER['REMOTE_ADDR'] . "&Refer=https://ptizapp.diw.go.th/login";
        $xml = simplexml_load_file($file);

        $json = json_encode($xml);
        $array = json_decode($json, TRUE);

        // echo '<pre>';
        // print_r($array);
        // echo '</pre>';
        // exit();

        if ($array['Return'] == "0") {

            $session = $array['SessionID'];
            $return = $array['Return'];
            $description = $array['Description'];
            $admin = $array['Sessions']['Admin'];
            $id_emp = $array['Sessions']['Id_Emp'];
            $id_org = $array['Sessions']['Id_Org'];
            $id_pos = $array['Sessions']['Id_Pos'];
            $id_Sorg = $array['Sessions']['Id_SOrg'];
            $idcard = $array['Sessions']['Idcard'];
            $levels = $array['Sessions']['Levels'];
            $user = $array['Sessions']['Login'];
            $name = $array['Sessions']['Name'];
            $pos_name = $array['Sessions']['Pos_name'];

            $select = $db->select()
                ->from('user')
                ->where(['user_code' => $id_emp]);

            $stmt = $db->prepareStatementForSqlObject($select);
            $results = $stmt->execute();

            if ($results->count()) {

                $userInfo = $results->current();

                $userInfo['user_position'] = $pos_name;
                $userInfo['user_name'] = $name;
                $userInfo['user_IDpos'] = $id_pos;
                $userInfo['user_levels'] = $levels;

                $this->save($userInfo, $userInfo['user_id']);
            } else {

                $userInfo['Description'] = $description;
                $userInfo['SessionID'] = $session;

                $userInfo['user_code'] = $id_emp;
                $userInfo['user_group_id'] = "14";

                $userInfo['user_idcard'] = $idcard;

                $userInfo['user_IDpos'] = $id_pos;
                $userInfo['user_levels'] = $levels;
                $userInfo['user_login'] = $user;
                $userInfo['user_name'] = $name;
                $userInfo['user_position'] = $pos_name;

                $this->save($userInfo, $userInfo['user_id']);
            }

            $select = $db->select()
                ->from('user')
                ->where(['user_code' => $id_emp]);

            $stmt = $db->prepareStatementForSqlObject($select);
            $results = $stmt->execute();

            $_SESSION['login'] = $results->current();

            //check admin
            $mGroup = new Group($this->factory);
            $groupInfo = $mGroup->get($_SESSION['login']['user_group_id']);
            $permission = json_decode($groupInfo['user_group_permission']);

            $_SESSION['login']['permission'] = $permission;

            //insert log

            if ($_SESSION['login']['user_id'] != 1) {
                $mLog = new Log($this->factory);
                $mLog->save($_SESSION['login']['user_id']);
            }


            return true;
        } else if ($array['Return'] == "2") {
            return false;
        } else {

            $select = $db->select()
                ->from('user')
                ->join('user_group', 'user.user_group_id = user_group.user_group_id', ['*'], 'LEFT')
                ->where(['user_login' => $login, 'user_password' => md5($password)])
                ->where('user_active = 1');

            // echo $db->buildSqlString($select);
            // exit();

            $stmt = $db->prepareStatementForSqlObject($select);
            $results = $stmt->execute();

            if (!$results->count()) {
                return false;
            }

            $_SESSION['login'] = $results->current();

            // print_r($_SESSION['login']);
            // exit();

            //check admin
            $mGroup = new Group($this->factory);
            $groupInfo = $mGroup->get($_SESSION['login']['user_group_id']);
            $permission = json_decode($groupInfo['user_group_permission']);

            $_SESSION['login']['permission'] = $permission;

            //insert log
            if ($_SESSION['login']['user_id'] != 1) {
                $mLog = new Log($this->factory);
                $mLog->save($_SESSION['login']['user_id']);
            }

            return true;
        }
    }

    public function isDuplicate($code)
    {
        $db = new Sql($this->adapter['read']);

        $code = substr($code, 1);

        $select = $db->select()
            ->from('user')
            ->where('user_active = 1')
            ->where("user_code = '" . $code . "'");

        // echo $db->buildSqlString($select);
        // exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        return $results->count();
    }

    public function paginator($search)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->from('user')
            ->join('user_group', 'user.user_group_id = user_group.user_group_id', ['user_group_name'], 'LEFT')
            ->where("(user_name LIKE '%" . $search . "%')")
            ->where(['user_active' => 1])
            ->where('user_id != 1');

        // echo $db->buildSqlString($select); exit();

        $resultSetPrototype = new ResultSet();

        $paginatorAdapter = new DbSelect(
            $select,
            $this->adapter['read'],
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function get($id = null)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->select()
            ->from('user')
            ->where('user_active = 1')
            ->order(['user_name' => 'ASC']);

        if (!is_null($id)) {
            $select->where(['user_id' => $id]);
        } else {
            $select->where('user_id != 1');
        }

        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        if (!is_null($id)) {
            return $results->current();
        } else {
            return $results;
        }
    }


    public function save($data, $id = null)
    {

        $table = new TableGateway('user', $this->adapter['write']);

        if (is_null($id)) {
            //insert
            $table->insert($data);
        } else {
            //update
            $table->update($data, ['user_id' => $id]);
        }
    }

    public function delete($id)
    {

        // $table = new TableGateway('user', $this->adapter['write']);
        // $table->delete(['user_id' => $id]);

        $this->save(['user_active' => 0], ['user_id' => $id]);
    }
}
