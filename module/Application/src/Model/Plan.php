<?php

namespace Application\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;

use Zend\Paginator\Paginator;

class Plan
{
    private $factory;
    private $adapter;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
        $this->adapter = $factory['adapter'];
    }

    public function paginator($search, $type = null, $activity = null, $planDate = null, $planDateTo  = null, $actualDate  = null, $actualDateTo  = null)
    {

        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->from('plan')
            ->join('station', 'plan.plan_station_road = station.station_road AND plan.plan_station_name = station.station_name', ['station_area'], 'LEFT')

            ->where("(plan_station_road LIKE '%" . $search . "%' OR plan_station_name LIKE '%" . $search . "%' OR plan_province LIKE '%" . $search . "%' OR plan_type LIKE '%" . $search . "%' OR plan_activity LIKE '%" . $search . "%')");

        if (isset($_SESSION['login']['permission']->admin) && $_SESSION['login']['permission']->admin != '1') {
            $select->where(['plan_team' => $_SESSION['login']['user_group_id']]);
        }

        if (!empty($type)) {
            $select->where(['plan.plan_type' => $type]);
        }

        if (!empty($activity)) {
            $select->where(['plan.plan_activity' => $activity]);
        }

        if (!empty($planDate)) {

            if (!empty($planDateTo)) {
                $select->where->between('plan.plan__at', $planDate, $planDateTo);
            } else {
                $select->where(['plan.plan_end_at' => $planDate]);
            }
        }
        if (!empty($actualDate)) {

            if (!empty($actualDateTo)) {
                $select->where->between('plan.plan_actual_at', $actualDate, $actualDateTo);
            } else {
                $select->where(['plan.plan_actual_at' => $actualDate]);
            }
        }

        // echo $db->buildSqlString($select);
        // exit();

        $resultSetPrototype = new ResultSet();

        $paginatorAdapter = new DbSelect(
            $select,
            $this->adapter['read'],
            $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function progressRoadTypeReport()
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->columns([
                'station_road',
            ])
            ->from('station')
            ->group(['station_road']);

        $stmt = $db->prepareStatementForSqlObject($select);
        $road = $stmt->execute();

        $data = [];
        foreach ($road as $row) {
            $select = $db->Select()
                ->columns([
                    'plan_station_road',
                    'plan_type',
                    'total' => new Expression('SUM(station_area)'),
                    'actual' => new Expression("SUM(IF(plan_actual_at IS NOT NULL, station_area, 0))"),
                ])
                ->from('plan')
                ->join('station', 'plan_station_road = station.station_road AND plan_station_name = station.station_name', [], 'LEFT')
                ->where(['plan_station_road' => $row['station_road']])
                ->group(['plan_type']);

            // echo $db->buildSqlString($select);
            // exit();

            $stmt = $db->prepareStatementForSqlObject($select);
            $results = $stmt->execute();

            foreach ($results as $res) {
                $data[] = $res;
            }
        }

        return $data;
    }

    public function progressRoadReport()
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->columns([
                'station_road',
            ])
            ->from('station')
            ->group(['station_road']);

        $stmt = $db->prepareStatementForSqlObject($select);
        $road = $stmt->execute();

        $data = [];
        foreach ($road as $row) {
            $select = $db->Select()
                ->columns([
                    'plan_station_road',
                    'total' => new Expression('SUM(station_area)'),
                    'actual' => new Expression("SUM(IF(plan_actual_at IS NOT NULL, station_area, 0))"),
                ])
                ->from('plan')
                ->join('station', 'plan_station_road = station.station_road AND plan_station_name = station.station_name', [], 'LEFT')
                ->where(['plan_station_road' => $row['station_road']])
                ->group(['plan_station_road']);


            $stmt = $db->prepareStatementForSqlObject($select);
            $results = $stmt->execute();

            $data[$row['station_road']] = $results->current();
        }

        return $data;
    }

    public function progressRegionReport()
    {
        $db = new Sql($this->adapter['read']);

        $region = [9, 10, 11, 12, 13];

        $data = [];
        foreach ($region as $id) {
            $select = $db->Select()
                ->columns([
                    'plan_team',
                    'total' => new Expression('SUM(station_area)'),
                    'topo_actual' => new Expression("SUM(IF(plan_actual_at IS NOT NULL AND plan_type = 'topo'  AND plan_team = " . $id . ", station_area, 0))"),
                    'ortho_actual' => new Expression("SUM(IF(plan_actual_at IS NOT NULL AND plan_type = 'ortho'  AND plan_team = " . $id . ", station_area, 0))"),
                    'gis_actual' => new Expression("SUM(IF(plan_actual_at IS NOT NULL AND plan_type = 'gis'  AND plan_team = " . $id . ", station_area, 0))"),
                ])
                ->from('plan')
                ->join('station', 'plan_station_road = station.station_road AND plan_station_name = station.station_name', [], 'LEFT')
                ->where(['plan_team' => $id])
                ->group(['plan_team']);

            // echo $db->buildSqlString($select);
            // exit();

            $stmt = $db->prepareStatementForSqlObject($select);
            $results = $stmt->execute();

            $data[$id] = $results->current();
        }

        return $data;
    }

    public function progressTypeReport()
    {
        $db = new Sql($this->adapter['read']);

        $select = $db->Select()
            ->columns(['plan_type', 'plan_activity'])
            ->from('plan')
            ->group(['plan_type', 'plan_activity']);

        // echo $db->buildSqlString($select);
        // exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();

        $data = [];

        foreach ($results as $row) {


            //get all
            $allQuery = $db->Select()
                ->columns(['all' => new Expression('SUM(station_area)')])
                ->from('plan')
                ->join('station', 'plan_station_road = station.station_road AND plan_station_name = station.station_name', [], 'LEFT')
                ->where(['plan_type' => $row['plan_type'], 'plan_activity' => $row['plan_activity']]);

            // echo $db->buildSqlString($select);
            // exit();

            $allStmt = $db->prepareStatementForSqlObject($allQuery);
            $allResults = $allStmt->execute();
            $all = $allResults->current();
            $row['all'] = $all['all'];


            //get plan
            $planQuery = $db->Select()
                ->columns(['plan' => new Expression('SUM(station_area)')])
                ->from('plan')
                ->join('station', 'plan_station_road = station.station_road AND plan_station_name = station.station_name', [], 'LEFT')
                ->where(['plan_type' => $row['plan_type'], 'plan_activity' => $row['plan_activity']])
                ->where(['plan_start_at IS NOT NULL', 'plan_end_at IS NOT NULL']);

            // echo $db->buildSqlString($select);
            // exit();

            $planStmt = $db->prepareStatementForSqlObject($planQuery);
            $planResults = $planStmt->execute();
            $plan = $planResults->current();
            $row['plan'] = $plan['plan'];

            //get actual
            $actualQuery = $db->Select()
                ->columns(['actual' => new Expression('SUM(station_area)')])
                ->from('plan')
                ->join('station', 'plan_station_road = station.station_road AND plan_station_name = station.station_name', [], 'LEFT')
                ->where(['plan_type' => $row['plan_type'], 'plan_activity' => $row['plan_activity']])
                ->where(['plan_actual_at IS NOT NULL']);

            // echo $db->buildSqlString($select);
            // exit();

            $actualStmt = $db->prepareStatementForSqlObject($actualQuery);
            $actualResults = $actualStmt->execute();
            $actual = $actualResults->current();
            $row['actual'] = $actual['actual'];


            $data[] = $row;
        }

        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // exit();

        return $data;
    }

    public function save($data, $id = null)
    {

        $table = new TableGateway('plan', $this->adapter['write']);

        if (is_null($id)) {
            //insert
            $table->insert($data);
        } else {
            //update
            $table->update($data, ['plan_id' => $id]);
        }
    }

    public function delete($id)
    {
        $table = new TableGateway('plan', $this->adapter['write']);

        $table->delete(['plan_id' => $id]);
    }
}
