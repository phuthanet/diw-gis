<?php
namespace Application\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Zend\Db\Adapter\Adapter;

use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

use Zend\I18n\Translator\Translator;

class DefaultControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get("Config");
        

        $readAdapter = new Adapter($config['db']['adapters']['ReadOnlyAdapter']);
        $writeAdapter = new Adapter($config['db']['adapters']['WriteAdapter']);

        //init translate
        $local = $config['translator']['locale'];
        $fallbackLocale = $config['translator']['fallbackLocale'];
        $type = $config['translator']['translation_file_patterns'][0]['type'];
        $baseDir = $config['translator']['translation_file_patterns'][0]['base_dir'];
        $pattern = $config['translator']['translation_file_patterns'][0]['pattern'];

        $translator = new Translator();
        $translator->addTranslationFilePattern($type, $baseDir, $pattern, 'default');
        $translator->setLocale($local);
        $translator->setFallbackLocale($fallbackLocale);

        //init smtp
        $transport = new SmtpTransport();
        $options   = new SmtpOptions($config['email']);
        $transport->setOptions($options);

        //init view
        $viewRenderer = $container->get('ViewRenderer');

        //init database
        $factory = [
            'adapter' => [
                'read' => $readAdapter,
                'write' => $writeAdapter,
            ],
            'config' => $config,
            'translator' => $translator,
            'view' => $viewRenderer,
            'email' => $transport,
            'code' => str_replace("pms_", "", $config['db']['adapters']['ReadOnlyAdapter']['database'])
        ];

        $arr = explode("\\", $requestedName);
        $controllerName = $arr[2];

        $class = '$controller = new \Application\Controller\\'.$controllerName.'($factory);';
        eval($class);

        return $controller;
    }
}
