<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

use Application\Model\Group;
use Application\Model\User;
use Application\Model\Layer;

use Utils\Utils;

class UserController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {
    }

    public function userinfoAction()
    {

        $code = $this->params()->fromQuery('code');
        $xml = simplexml_load_file('http://www3.diw.go.th/session/InfoAdmin.asp?Login=U' . $code);

        // print_r($xml);

        $code = $xml->Login;

        //check user duplicate
        $mUser = new User($this->factory);
        $duplicate = $mUser->isDuplicate($code);

        $xml->duplicate = $duplicate;

        $json = json_encode($xml, JSON_UNESCAPED_UNICODE);
        // $user = json_decode($json, TRUE);

        echo $json;


        exit();
    }

    public function addAction()
    {

        $user_id = $this->params()->fromPost('user_id');
        $user_code = $this->params()->fromPost('user_code');
        $user_name = $this->params()->fromPost('user_name');
        $user_login = $this->params()->fromPost('user_login');
        $user_password = $this->params()->fromPost('user_password');
        $user_position = $this->params()->fromPost('user_position');
        $user_gender = $this->params()->fromPost('user_gender');
        $user_phone = $this->params()->fromPost('user_phone');
        $user_email = $this->params()->fromPost('user_email');
        $user_group_id = $this->params()->fromPost('user_group_id');

        $data = [
            'user_code' => $user_code,
            'user_name' => $user_name,
            'user_login' => $user_login,
            'user_password' => md5($user_password),
            'user_position' => $user_position,
            'user_phone' => $user_phone,
            'user_email' => $user_email,
            'user_group_id' => $user_group_id,
        ];

        if (empty($user_password)) {
            unset($data['user_password']);
        }

        $mUser = new User($this->factory);
        $mUser->save($data, $user_id);

        exit();
    }

    public function addpermissionAction()
    {

        $user_id = $this->params()->fromPost('user_id');

        $user_code = $this->params()->fromPost('user_code');
        $user_name = $this->params()->fromPost('user_name');
        $user_login = $this->params()->fromPost('user_login');
        $user_position = $this->params()->fromPost('user_position');
        $user_email = $this->params()->fromPost('user_email');
        $user_group_id = $this->params()->fromPost('user_group_id');

        $data = [
            'user_code' => $user_code,
            'user_name' => $user_name,
            'user_login' => $user_login,
            'user_position' => $user_position,
            'user_email' => $user_email,
            'user_group_id' => $user_group_id,
        ];


        if ($user_id) {
            unset($data['user_name']);
        }

        $mUser = new User($this->factory);
        $mUser->save($data, $user_id);

        exit();
    }

    public function infoAction()
    {
        $user_id = $this->params()->fromPost('user_id');
        $mUser = new User($this->factory);
        $user = $mUser->get($user_id);

        echo json_encode($user);

        exit();
    }

    public function deleteAction()
    {

        $user_id = $this->params()->fromPost('user_id');
        $mUser = new User($this->factory);
        $mUser->delete($user_id);
        exit();
    }

    public function onDispatch(MvcEvent $e)
    {

        if (!isset($_SESSION['login'])) {
            return $this->redirect()->toRoute('login', ['action' => 'index']);
        }

        $response = parent::onDispatch($e);

        return $response;
    }
}
