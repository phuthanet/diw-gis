<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;


use Application\Model\User;
use Application\Model\Log;
use Application\Model\Group;
use Application\Model\Layer;
use Application\Model\Report;
use Utils\Utils;

class AdminController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {
    }

    public function logAction()
    {

        $this->layout()->menu = 'admin/log';

        $page = (int) $this->params()->fromQuery('page', 1);
        $search = $this->params()->fromQuery('search', '');

        $model = new Log($this->factory);

        $paginator = $model->paginator($search);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        $paginator->setPageRange(4);

        $number = $paginator->getItemCountPerPage();

        return new ViewModel(array(
            'paginator' => $paginator,
            'page' => $page,
            'search' => $search,
            'number' => $number,
        ));
    }

    public function groupAction()
    {
        $this->layout()->menu = 'admin/group';

        $page = (int) $this->params()->fromQuery('page', 1);
        $search = $this->params()->fromQuery('search', '');

        $model = new Group($this->factory);

        $paginator = $model->paginator($search);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        $paginator->setPageRange(4);

        $number = $paginator->getItemCountPerPage();

        return new ViewModel(array(
            'paginator' => $paginator,
            'page' => $page,
            'search' => $search,
            'number' => $number,
        ));
    }

    public function layerinfoAction()
    {
        $layerId = $this->params()->fromPost('layer_id');

        $mLayer = new Layer($this->factory);
        $layer = $mLayer->get($layerId);

        echo json_encode($layer);

        exit();
    }

    public function attributeAction()
    {
        $layerId = $this->params()->fromPost('layer_id');

        $mLayer = new Layer($this->factory);
        $layer = $mLayer->get($layerId);

        echo json_encode($layer);

        exit();
    }

    public function saveattrAction()
    {
        $layerId = $this->params()->fromPost('layer_id');
        $attr = $this->params()->fromPost('attribute');
        $mLayer = new Layer($this->factory);
        $mLayer->saveAttribute($attr, $layerId);
        exit();
    }

    public function layerAction()
    {

        $model = new Layer($this->factory);

        if ($this->getRequest()->isPost()) {

            $action = $this->params()->fromPost('action');
            if ($action == 'sort') {

                $layer_id = $this->params()->fromPost('layer_id');
                $sort = $this->params()->fromPost('sort');
                $model->sort($layer_id, $sort);
                exit();
            }
            if ($action == 'remove') {

                $layer_id = $this->params()->fromPost('layer_id');
                $model->delete($layer_id);
                exit();
            }

            if ($action == 'add') {

                $name = $this->params()->fromPost('layer_name');
                $type = $this->params()->fromPost('layer_type');
                $url = $this->params()->fromPost('layer_url');
                $group = $this->params()->fromPost('layer_group_id');

                $data = [
                    'layer_name' => $name,
                    'layer_type' => $type,
                    'layer_url' => $url,
                    'layer_group_id' => $group,
                ];

                $id = $model->save($data);

                $data = [
                    'layer_order' => $model->getLastOrder() + 1,
                ];
                //update order
                $model->save($data, $id);

                return $this->redirect()->toRoute('admin', ['action' => 'layer']);
                exit();
            }
            if ($action == 'edit') {

                $id = $this->params()->fromPost('layer_id');
                $name = $this->params()->fromPost('layer_name');
                $type = $this->params()->fromPost('layer_type');
                $url = $this->params()->fromPost('layer_url');
                $group = $this->params()->fromPost('layer_group_id');

                $data = [
                    'layer_name' => $name,
                    'layer_type' => $type,
                    'layer_url' => $url,
                    'layer_group_id' => $group,
                ];

                $model->save($data, $id);
                return $this->redirect()->toRoute('admin', ['action' => 'layer']);
                exit();
            }
        }

        $this->layout()->menu = 'admin/layer';

        $page = (int) $this->params()->fromQuery('page', 1);
        $search = $this->params()->fromQuery('search', '');

        $lists = $model->lists($search);

        $groups = $model->getGroupLayer(null, 'ASC');
        $groupLayer = [];
        foreach ($groups as $row) {
            $groupLayer[] = $row;
        }

        return new ViewModel(array(
            'lists' => $lists,
            'page' => $page,
            'search' => $search,
            'groupLayer' => $groupLayer,
        ));
    }

    public function userAction()
    {
        $this->layout()->menu = 'admin/user';

        $page = (int) $this->params()->fromQuery('page', 1);
        $search = $this->params()->fromQuery('search', '');

        $model = new User($this->factory);

        $paginator = $model->paginator($search);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        $paginator->setPageRange(4);

        $number = $paginator->getItemCountPerPage();

        //get group permission
        $mGroup = new Group($this->factory);
        $group = $mGroup->get();

        return new ViewModel(array(
            'paginator' => $paginator,
            'page' => $page,
            'search' => $search,
            'number' => $number,
            'group' => $group,
        ));
    }

    public function permissionAction()
    {
        $this->layout()->menu = 'admin/permission';

        $page = (int) $this->params()->fromQuery('page', 1);
        $search = $this->params()->fromQuery('search', '');

        $model = new User($this->factory);

        $paginator = $model->paginator($search);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        $paginator->setPageRange(4);

        $number = $paginator->getItemCountPerPage();

        //get group permission
        $mGroup = new Group($this->factory);
        $group = $mGroup->get();

        return new ViewModel(array(
            'paginator' => $paginator,
            'page' => $page,
            'search' => $search,
            'number' => $number,
            'group' => $group,
        ));
    }

    public function groupinfoAction()
    {

        $groupId = $this->params()->fromPost('group_id', 0);

        //get group info
        $mGroup = new Group($this->factory);
        $groupInfo = $mGroup->get($groupId);
        $permission = json_decode($groupInfo['user_group_permission']);



        //get layer
        $mLayer = new Layer($this->factory);
        $res = $mLayer->getGroupLayer();

        $layerGroup = [];
        foreach ($res as $row) {

            //get layer
            $layers = $mLayer->getLayersByGroup($row['layer_group_id']);

            foreach ($layers as $layer) {
                $checked = '';
                if (isset($permission->layers) && in_array($layer['layer_id'], $permission->layers)) {
                    $checked = 'checked';
                }
                $layer['checked'] = $checked;
                $row['layers'][] = $layer;
            }

            $layerGroup[] = $row;
        }

        $groupInfo['group_layers'] = $layerGroup;

        echo json_encode($groupInfo);

        exit();
    }

    public function editgroupAction()
    {

        $groupId = $this->params()->fromPost('group_id', 0);
        $groupName = $this->params()->fromPost('user_group_name', 0);
        $admin = $this->params()->fromPost('admin', 0);
        $editLayer = $this->params()->fromPost('edit_layer', 0);
        $subcontractor = $this->params()->fromPost('subcontractor', 0);
        $layers = $this->params()->fromPost('layers');

        $mGroup = new Group($this->factory);

        $permission = [
            'admin' => $admin,
            'edit_layer' => $editLayer,
            'subcontractor' => $subcontractor,
            'layers' => $layers
        ];

        $data = [
            'user_group_name' => $groupName,
            'user_group_permission' => json_encode($permission)
        ];

        $mGroup->save($data, $groupId);

        exit();
    }

    public function reportAction()
    {
        $this->layout()->menu = 'admin/report';

        $mReport = new Report($this->factory);

        $report = $mReport->getreport();

        // print_r($report);
        // exit();

        return new ViewModel(array(
            'report' => $report,
        ));
    }

    public function searchAction()
    {
        $this->layout()->menu = 'admin/search';

        $mReport = new Report($this->factory);

        $f_search = $mReport->getsearch();

        return new ViewModel(array(
            'f_search' => $f_search,
        ));
    }

    public function onDispatch(MvcEvent $e)
    {

        if (!isset($_SESSION['login'])) {
            return $this->redirect()->toRoute('login', ['action' => 'index']);
        }

        $response = parent::onDispatch($e);

        return $response;
    }
}
