<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

// use Application\Model\Jobs;

use Utils\Utils;

class NontargetController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {


        // $user = $this->forward()->dispatch('user', ['action' => 'detail', 'from' => 'dashboard']);
        $this->layout()->setTemplate('layout/nontarget');
    }

    public function onDispatch(MvcEvent $e)
    {


        $response = parent::onDispatch($e);

        return $response;
    }
}
