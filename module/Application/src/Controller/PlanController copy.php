<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

// use Application\Model\Jobs;
use Application\Model\Layer;
use Application\Model\Plan;

use Utils\Utils;

class PlanController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {
        $layerId = $this->params()->fromQuery('layer_id');

        $mLayer = new Layer($this->factory);
        $layerInfo = $mLayer->get($layerId);

        $groupId = $_SESSION['login']['user_group_id'];
        $editLayerInfo = $mLayer->getEditLayerInfo($layerId, $groupId);

        $primaryKey = $editLayerInfo['edit_layer_primary_key'];
        $layerKey = json_decode($editLayerInfo['edit_layer_key']);
        $displayName = $editLayerInfo['edit_layer_display_field'];
        $displayCode = $editLayerInfo['edit_layer_primary_key'];

        //get activity list
        $mPlan = new Plan($this->factory);
        $activityList = $mPlan->getActivityList();

        return new ViewModel(array(
            'layerInfo' => $layerInfo,
            'primaryKey' => $primaryKey,
            'layerKey' => $layerKey,
            'displayName' => $displayName,
            'displayCode' => $displayCode,
            'activityList' => $activityList,
        ));
    }

    public function listAction()
    {
        $this->layout()->setTemplate('layout/none');
        $code = $this->params()->fromPost('code');

        $mPlan = new Plan($this->factory);
        $plan = $mPlan->getPlanByCode($code);

        return new ViewModel(array(
            'code' => $code,
            'plan' => $plan,
        ));
    }

    public function saveAction()
    {

        $code = $this->params()->fromPost('code');
        $activityId = $this->params()->fromPost('activity');
        $startDate = $this->params()->fromPost('start_date');
        $endDate = $this->params()->fromPost('end_date');
        $actualDate = $this->params()->fromPost('actual_date');

        $planId = $this->params()->fromPost('plan_id');


        if (empty($startDate)) {
            $startDate = null;
        }
        if (empty($endDate)) {
            $endDate = null;
        }
        if (empty($actualDate)) {
            $actualDate = null;
        }

        $data = [
            'code' => $code,
            'plan_activity_id' => $activityId,
            'plan_start_at' => $startDate,
            'plan_end_at' => $endDate,
            'plan_actual_at' => $actualDate,
            'plan_update_by' => $_SESSION['login']['user_id'],
            'plan_update_at' => date('Y-m-d H:i:s'),
        ];

        $mPlan = new Plan($this->factory);
        $mPlan->save($data, $planId);

        exit();
    }

    public function deleteAction()
    {
        $planId = $this->params()->fromPost('id');
        $mPlan = new Plan($this->factory);
        $mPlan->delete($planId);
        exit();
    }

    public function onDispatch(MvcEvent $e)
    {

        if (!isset($_SESSION['login'])) {
            return $this->redirect()->toRoute('login', ['action' => 'index']);
        }

        $this->layout()->menu = 'plan';

        $response = parent::onDispatch($e);

        return $response;
    }
}
