<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

use Application\Model\Group;
use Application\Model\User;
use Application\Model\Layer;
use SoapClient;

use Utils\Utils;

class ApiController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {
    }

    public function soapAction()
    {
        $fid = $this->params()->fromQuery('faccode', '');
        $index = $this->params()->fromQuery('type', '');
        $wsdl =  "http://diwws.diw.go.th/diwFID/FacFID?wsdl";
        $client = new SoapClient($wsdl, array(
            "trace" => 1,
            "exceptions" => 0,
        ));
        $parameter = array('parameter' => array('clientId' => 'PTIZ2564iw*', 'FID' => $fid));
        $response = $client->diwfacFID($parameter);

        $factory = $response->return->FAC;
        $FID = $factory->FID;
        $Facreg = $factory->FACREG;

        $num = "0,1,2,3,4,5,6,7,8,9";
        $strlen = strlen($Facreg);
        $TxtOnly = "";
        $NumOnly = "";
        for ($i = 0; $i < $strlen; $i++) {
            $item = substr($Facreg, $i, 1);
            if (strstr($num, $item)) {
                $TxtOnly .= $item;
            } else {
                $NumOnly .= $item;
            }
        }

        if ($index == 1) {
            header('location:http://reg.diw.go.th/executive/waste.asp?FID=' . $FID);
        } else if ($index == 2) {
            header('location:http://reg.diw.go.th/executive/result_FACFID.asp?Type=4&FID=' . $FID);
        } else if ($index == 3) {
            header('location:http://reg.diw.go.th/executive/result_FACFID.asp?Type=1&FID=' . $FID);
        } else if ($index == 4) {
            header('location:http://reg.diw.go.th/executive/result_FACFID.asp?Type=2&FID=' . $FID);
        } else if ($index == 5) {
            header('location:http://reg.diw.go.th/executive/result_FACFID.asp?Type=3&FID=' . $FID);
        } else if ($index == 6) {
            header('location:http://reg.diw.go.th/boiler/pot_boiler_Search.asp?comm=1&PB01_FAC_REG=' . $TxtOnly);
        } else if ($index == 7) {
            header('location:http://reg.diw.go.th/executive/FacSurvey.asp?FID=' . $FID);
        } else if ($index == 8) {
            header('location:http://reg.diw.go.th/diwrisk/report_facreg.asp?FID=' . $FID);
        }

        exit();
    }


    public function onDispatch(MvcEvent $e)
    {

        $response = parent::onDispatch($e);

        return $response;
    }
}
