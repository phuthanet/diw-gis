<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

use Application\Model\Layer;
use Application\Model\Group;
use Application\Model\Map;

use Utils\Utils;

class MapController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {

        // print_r($_SESSION);
        // exit();
        $mGroup = new Group($this->factory);
        $groupInfo = $mGroup->get($_SESSION['login']['user_group_id']);
        $permission = json_decode($groupInfo['user_group_permission']);

        $mMap = new Map($this->factory);
        $search = $mMap->getSearch();
        // print_r($search);
        // exit();
        $mLayer = new Layer($this->factory);

        $res = $mLayer->getGroupLayer();

        $layerGroup = [];
        foreach ($res as $row) {

            //get layer
            $layers = $mLayer->getLayersByGroup($row['layer_group_id']);

            foreach ($layers as $layer) {

                if (isset($permission->layers) && in_array($layer['layer_id'], $permission->layers)) {
                    $row['layers'][] = $layer;
                }
            }

            $layerGroup[] = $row;
        }
        //  print_r($layerGroup);
        //  exit();

        return new ViewModel(array(
            'layerGroup' => $layerGroup,
            'search' => $search
        ));
    }

    public function advancesearchAction()
    {

        $this->layout()->setTemplate('layout/none');

        $data = $this->params()->fromPost('data', []);

        if (
            $data['boiler'] == '' &&
            $data['hazard_waste'] == '' &&
            $data['non_hazard_waste'] == '' &&
            $data['machine'] == '' &&
            $data['material'] == '' &&
            $data['product'] == ''
        ) {
            echo json_encode(['status' => 500, 'message' => 'error']);
            exit();
        }

        $mMap = new Map($this->factory);
        $results = $mMap->advanceSearch($data);

        $factory = [];
        foreach ($results as $row) {

            if (!in_array($row['factory_code'], $factory)) {
                $factory[] = $row['factory_code'];
            }
        }

        echo json_encode(['status' => 200, 'message' => 'success', 'data' => $factory], JSON_UNESCAPED_UNICODE);

        exit();
    }

    public function apiAction()
    {
        $dispfacreg = trim($this->params()->fromQuery('dispfacreg'));
        $fid = trim($this->params()->fromQuery('fid'));

        $dispfacreg = urlencode($dispfacreg);
        $fid = urlencode($fid);
        
        // $dispfacreg = 'ข3-72-2/38อย';
        // $fid = '91600100225389';
        $year = date('Y')+543;
        
        // Build the URL with the dynamic values
        $url = "https://facchem.diw.go.th/api/report/internal/data?year={$year}&dispfacreg={$dispfacreg}&fid={$fid}&loc_code=&reg_no=";

        // echo $url;exit();
    
        // Set the headers
        $headers = [
            'DIW-Token' => 'KUd6Z9dfJV2h0HV',
        ];
    
        // Create an instance of the HTTP client
        $client = new \Zend\Http\Client();
        $client->setUri($url);
        $client->setHeaders($headers);
        $client->setOptions([
            'sslverifypeer' => false, // Set to true if you need to verify the SSL certificate
        ]);
    
        // Send the request and get the response
        try {
            $response = $client->send();
        } catch (\Throwable $th) {
            // throw $th;
            echo 'ใช้เวลาในการประมวลผลนานเกินไป';
            exit();
        }
        
    
        // Check if the request was successful (status code 200)
        if ($response->getStatusCode() == 200) {
            // Get the response body
            $responseData = $response->getBody();
    
            // Process the response data as needed
            $apiData = json_decode($responseData, true);

            if(!is_array($apiData)){
                $apiData = [];
            }
            
            // Pass the API data to the view template
            return new ViewModel([
                'apiData' => $apiData,
                'url' => $url,
            ]);
        } else {
            // Handle the case where the API request failed
            $errorMessage = 'ไม่มีข้อมูล';
            exit();
            
            // Pass the error message to the view template
            return new ViewModel([
                'errorMessage' => $errorMessage,
                'url' => $url,

            ]);
        }
    }
    
    

    public function onDispatch(MvcEvent $e)
    {

        if (!isset($_SESSION['login'])) {
            return $this->redirect()->toRoute('login', ['action' => 'index']);
        }

        $this->layout()->menu = 'home';

        $response = parent::onDispatch($e);

        return $response;
    }
}
