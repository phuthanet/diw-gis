<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

use Application\Model\Layer;
use Application\Model\Group;
use Application\Model\Report;

use Utils\Utils;

class ReportController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {
    }

    public function addAction()
    {

        $report_id = $this->params()->fromPost('report_id');
        $report_name = $this->params()->fromPost('report_name');
        $report_link = $this->params()->fromPost('report_link');


        $data = [
            'report_name' => $report_name,
            'report_link' => $report_link,

        ];


        $mReport = new Report($this->factory);
        $mReport->save($data, $report_id);

        exit();
    }

    public function deleteAction()
    {
        $id = $this->params()->fromPost('report_id');
        $mReport = new Report($this->factory);
        $mReport->delete($id);
        exit();
    }

    public function addSearchAction()
    {

        $search_id = $this->params()->fromPost('search_id');
        $search_name = $this->params()->fromPost('search_name');
        $search_url = $this->params()->fromPost('search_url');
        $search_field = $this->params()->fromPost('search_field');
        $placeholder = $this->params()->fromPost('placeholder');


        $data = [
            'search_name' => $search_name,
            'search_url' => $search_url,
            'search_field' => $search_field,
            'placeholder' => $placeholder,

        ];


        $mReport = new Report($this->factory);
        $mReport->saveSearch($data, $search_id);

        exit();
    }

    public function deleteSearchAction()
    {
        $id = $this->params()->fromPost('search_id');
        $mReport = new Report($this->factory);
        $mReport->deleteSearch($id);
        exit();
    }

    public function onDispatch(MvcEvent $e)
    {

        if (!isset($_SESSION['login'])) {
            return $this->redirect()->toRoute('login', ['action' => 'index']);
        }

        $response = parent::onDispatch($e);

        return $response;
    }
}
