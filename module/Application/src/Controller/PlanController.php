<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Mvc\MvcEvent;

use Zend\Mvc\Controller\Plugin\Forward;
use Zend\View\Renderer\PhpRenderer;

use Application\Model\Plan;

use Utils\Utils;

class PlanController extends AbstractActionController
{
    private $factory;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
    }

    public function indexAction()
    {
        $page = (int) $this->params()->fromQuery('page', 1);
        $search = $this->params()->fromQuery('search', '');
        $type = $this->params()->fromQuery('type', '');
        $activity = $this->params()->fromQuery('activity', '');

        $planDate = $this->params()->fromQuery('plan_date', '');
        $planDateTo = $this->params()->fromQuery('plan_date_to', '');
        $actualDate = $this->params()->fromQuery('actual_date', '');
        $actualDateTo = $this->params()->fromQuery('actual_date_to', '');

        $model = new Plan($this->factory);

        $paginator = $model->paginator($search, $type, $activity, $planDate, $planDateTo, $actualDate, $actualDateTo);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(50);
        $paginator->setPageRange(5);

        $number = $paginator->getItemCountPerPage();

        $typeList = ['topo', 'ortho', 'gis'];

        $activityList = ['สำรวจภาคสนาม', 'เขียน CAD', 'บินโดรน', 'shp', 'attribute'];

        return new ViewModel(array(
            'paginator' => $paginator,
            'page' => $page,
            'search' => $search,
            'type' => $type,
            'activity' => $activity,
            'number' => $number,
            'typeList' => $typeList,
            'activityList' => $activityList,
            'planDate' => $planDate,
            'planDateTo' => $planDateTo,
            'actualDate' => $actualDate,
            'actualDateTo' => $actualDateTo,

        ));
    }

    public function reportAction()
    {

        $mPlan = new Plan($this->factory);

        $type = $mPlan->progressTypeReport();

        $region = $mPlan->progressRegionReport();

        $road = $mPlan->progressRoadReport();

        $roadType = $mPlan->progressRoadTypeReport();

        $sort = [];
        foreach ($road as $key => $value) {
            if ($value['total']) {
                $sort[$key] =  ($value['actual'] / $value['total']) * 100;
            } else {
                $sort[$key] = 0;
            }
        }

        arsort($sort);

        $roadData = [];
        foreach ($sort as $key => $value) {
            $roadData[$key] = $road[$key];
        }

        // echo '<pre>';
        // print_r($sort);
        // echo '</pre>';

        // echo '<pre>';
        // print_r($region);
        // echo '</pre>';
        // exit();

        return new ViewModel(array(
            'type' => $type,
            'region' => $region,
            'road' => $roadData,
            'roadType' => $roadType
        ));
    }

    public function saveAction()
    {
        $id = $this->params()->fromPost('id');
        $field = $this->params()->fromPost('field');
        $value = $this->params()->fromPost('value');

        $mPlan = new Plan($this->factory);

        $data[$field] = $value;
        $mPlan->save($data, $id);

        echo $field;
        exit();
    }

    public function onDispatch(MvcEvent $e)
    {

        if (!isset($_SESSION['login'])) {
            return $this->redirect()->toRoute('login', ['action' => 'index']);
        }

        $this->layout()->menu = 'plan';

        $response = parent::onDispatch($e);

        return $response;
    }
}
