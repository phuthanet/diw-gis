<?php
namespace Utils;

use Application\Model\Evaluate;

use Zend\Db\TableGateway\TableGateway;

use Application\Model\License;

use Zend\Db\Sql\Sql;
use Zend\Math\Rand;

use Zend\I18n\Translator\Translator;

use Zend\Cache\StorageFactory;

class Utils
{
    private $factory;
    private $adapter;
    protected $translator;

    public function __construct(array $factory)
    {
        $this->factory = $factory;
        $this->adapter = $factory['adapter'];
        $this->translator = $factory['translator'];

        if (!defined('API_ACCESS_KEY')) {
            define('API_ACCESS_KEY', 'AAAAIyf6Bwk:APA91bGAoNDJo8YYzT6Ajj7pruQSO3WQTjjs5vLL7fNs1AzyKjluMNjcGMVAeE6rNvsxBR5_anDDi2hsnXk5SwJQ8tkAjQp0YkM17dnSSM1458tAymSlsIifz4OQBNPyOSQLUBbmAveA');
        }
        
    }

    public function pushNotification($token, $title, $body, $data = null){

        $data = [
            "to" => $token,
            "notification" => [
                "title" => $title,
                "body" => $body,
                "icon" => "https://pms.servicesf1.com/web/images/logo.png",
                // "click_action" => "https://pms.servicesf1.com"
            ],
            "data" => $data,
        ];

        $data_string = json_encode($data);

        $headers = array(
        'Authorization: key=' . API_ACCESS_KEY,
        'Content-Type: application/json'
        );

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

        $result = curl_exec($ch);
        $err = curl_error($ch);

        curl_close($ch);

        if ($err) {
            return $err;
        }

        return $result;

    }

    public function checkFeature($name){

        if(isset($_COOKIE['pmscode'])){

            $code = $_COOKIE['pmscode'];
            $code = strtolower($code);

            $mLicense = new License($this->factory);

            $license = $mLicense->get($code);


            if(isset($license->license_feature)){

                $obj = json_decode($license->license_feature);

                eval('$feature = isset($obj->'.$name.');');

                return $feature;

            }
                
        }
        
        return false;

    }

    public function thumb($url){

        $image = '<img style="height: 100px; max-width: 100px;" src="'.$url.'"/>';

        return $image;
    }

    public function diffDate($start, $end){
        if(strtotime($start) >= -25200){
                    
            $startDay = new \DateTime(date('d.m.Y', strtotime($start)));

            if(empty($end)){
                $end = new \DateTime();
            }else{
                $end = new \DateTime(date('d.m.Y', strtotime($end)));
            }
            
            $diff = $end->diff($startDay);

            $age = '';
            if($diff->y > 0){
                $age .= $diff->y.' '.$this->translator->translate('year');
            }
            if($diff->m > 0){
                $age .= ' '.$diff->m.' '.$this->translator->translate('month');
            }elseif($diff->y < 1){
                $age = $diff->d.' '.$this->translator->translate('Day');
            }
            return $age;
        }else{
            return $this->translator->translate('Unknow');
        }
    }

    public function checkSystemExpire(){

        if(isset($_COOKIE['pmscode'])){

            $code = $_COOKIE['pmscode'];
            $code = strtolower($code);

            $mLicense = new License($this->factory);

            $license = $mLicense->get($code);

            if(isset($license->license_id)){

                if($license->license_active == 1){

                    if(strtotime($license->license_expire) > time()){
                        
                        return false;
                        // return $this->redirect()->toRoute('application', ['action' => 'index', 'params' => []]);
                    }else{
                        return '<center>System expired at '.$license->license_expire.'. Please contact Tel:0859406645 or Email:saichon@chonsolutions.com</center>';
                    }
                    
                }else{
                    return '<center>System not active. Please contact Tel:0859406645 or Email:saichon@chonsolutions.com</center>';
                }

            }
                
        }
        
        return false;
    }

    public function checkUserActive(){

        if(isset($_COOKIE['pmsulimit'])){
            $token = $_COOKIE['pmsulimit'];
            
            $table = new TableGateway('user_active', $this->adapter['write']);
            // $table->update(['user_act_timestamp' => date('Y-m-d H:i:s')], ['user_id' => $this->getUserId(), 'user_act_token' => $token]);
            
            $rowset = $table->select(['user_id' => $this->getUserId(), 'user_act_token' => $token]);
            if($rowset->count()){
                return true;
            }
        }

        return false;

    }

    public function getBadges($userId){

        $mEvaluate = new Evaluate($this->factory);
        $badges = [];
        $badges['evaluate'] = $mEvaluate->getEvaluateByUser($userId, 'count');

        return $badges;
    }

    public function genPassword(){

        $password = '';
        for($i = 0; $i < 6; $i++){
            $password .= Rand::getString(1, 'abcdefghijklmnopqrstuvwxyz0123456789');
        }
        return $password;

    }

    public function getMonth(){

        $month = [
            'en' => [
                '01' => 'January',
                '02' => 'February',
                '03' => 'March',
                '04' => 'April',
                '05' => 'May',
                '06' => 'June',
                '07' => 'July',
                '08' => 'August',
                '09' => 'September',
                '10' => 'October',
                '11' => 'November',
                '12' => 'December',
            ],
            'th' => [
                '01' => 'มกราคม',
                '02' => 'กุมภาพันธ์',
                '03' => 'มีนาคม',
                '04' => 'เมษายน',
                '05' => 'พฤษภาคม',
                '06' => 'มิถุนายน',
                '07' => 'กรกฎาคม',
                '08' => 'สิงหาคม',
                '09' => 'กันยายน',
                '10' => 'ตุลาคม',
                '11' => 'พฤศจิกายน',
                '12' => 'ธันวาคม',
            ],
        ];
        if(isset($month[$this->getLanguage()])){
            return $month[$this->getLanguage()];
        }else{
            return $month['en'];
        }
        
    }

    private function getShotMonth(){
        $month = [
            'en' => [
                '01' => 'Jan',
                '02' => 'Feb',
                '03' => 'Mar',
                '04' => 'Apr',
                '05' => 'May',
                '06' => 'Jun',
                '07' => 'Jul',
                '08' => 'Aug',
                '09' => 'Sep',
                '10' => 'Oct',
                '11' => 'Nov',
                '12' => 'Dec',
            ],
            'th' => [
                '01' => 'ม.ค',
                '02' => 'ก.พ',
                '03' => 'มี.ค',
                '04' => 'เม.ย',
                '05' => 'พ.ค',
                '06' => 'มิ.ย',
                '07' => 'ก.ค',
                '08' => 'ส.ค',
                '09' => 'ก.ย',
                '10' => 'ต.ค',
                '11' => 'พ.ย',
                '12' => 'ธ.ค',
            ],
        ];

        if(isset($month[$this->getLanguage()])){
            return $month[$this->getLanguage()];
        }else{
            return $month['en'];
        }
    }

    public function formatDate($date, $option = null, $timeFormat = null){

        if(empty($date)){
            return $this->translator->translate('Unknow');
        }

        if($option == 'shotmonth'){
            $month = $this->getShotMonth();
        }else{
            $month = $this->getMonth();
        }

        $d = strtotime($date);

        $time = '';
        if(!is_null($timeFormat)){
            $time = ' '.date($timeFormat, $d);
        }

        return date('d', $d).' '.$month[date('m', $d)].' '.(date('Y', $d)+543).$time;
    }

    public function getApplicationPath(){
        return realpath(__DIR__).'/../';
    }

    public function getPublicPath(){
        return realpath(__DIR__).'/../public';
    }

    public function evalHash($evaluationId, $userId, $assessorId){
        return md5($evaluationId.$userId.$assessorId.$this->getSystemKey());
    }

    public function log($message, $priorityName = 'INFO' , $uid = null){

        // EMERG   = 0;  // Emergency: system is unusable
        // ALERT   = 1;  // Alert: action must be taken immediately
        // CRIT    = 2;  // Critical: critical conditions
        // ERR     = 3;  // Error: error conditions
        // WARN    = 4;  // Warning: warning conditions
        // NOTICE  = 5;  // Notice: normal but significant condition
        // INFO    = 6;  // Informational: informational messages
        // DEBUG   = 7;  // Debug: debug messages

        $db = new Sql($this->adapter['write']);

        if(is_null($uid) ){
            $uid = $this->getUserId();
        }

        
        switch ($priorityName) {
            case 'EMERG':
                $priority = 0;
                break;
            case 'ALERT':
                $priority = 1;
                break;
            case 'CRIT':
                $priority = 2;
                break;
            case 'ERR':
                $priority = 3;
                break;
            case 'WARN':
                $priority = 4;
                break;
            case 'NOTICE':
                $priority = 5;
                break;
            case 'INFO':
                $priority = 6;
                break;
            case 'DEBUG':
                $priority = 7;
                break;
            case 'ACCESS':
                $priority = 8;
                break;
            
            default:
                $priorityName = 'INFO';
                $priority = 6;
                break;
        }
      
        $data['log_message'] = $message;
        $data['log_priority'] = $priority;
        $data['log_priority_name'] = $priorityName;
        $data['log_timestamp'] = date('Y-m-d H:i:s');
        $data['user_id'] = $uid;

        // print_r($data);exit();

        $insert = $db->insert()
        ->into('system_logs')
        ->values($data);

        $stmt = $db->prepareStatementForSqlObject($insert);
        $results = $stmt->execute();
 

    }
    public function getSystemKey(){
        $db = new Sql($this->adapter['read']);

        $select = $db->select()
        ->from('system_config')
        ->where('config_id = 1');
        
        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();
        $res = $results->current();

        return $res['config_key'];
            
    }
    public function getUserId(){
    
        if(isset($_COOKIE['pmsuid'])){
            $id = $_COOKIE['pmsuid'];
            $hash = $_COOKIE['pmsuhash'];
            $code = $_COOKIE['pmscode'];
            // echo md5($id.$code);exit();
            if($hash == md5($id.$code)){
                return $id;
            }
        }
        
        return false;
    }

    public function getLanguage(){
        $db = new Sql($this->adapter['read']);

        $select = $db->select()
        ->from('user')
        ->where(['user_id' => $this->getUserId() ]);
        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();
        $data = $results->current();

        if(!empty($data['user_language'])){
            setcookie("pmslang", $data['user_language'], time() + (86400 * 30), "/");
            return $data['user_language'];
        }else{
            setcookie("pmslang", 'en', time() + (86400 * 30), "/");
            return 'en';
        }

    }

    public function setLanguage($code){
        $table = new TableGateway('user', $this->adapter['write']);
        $table->update(['user_language' => $code], ['user_id' => $this->getUserId()]);
        setcookie("pmslang", $code, time() + (86400 * 30), "/");

    }

    public function getSiteInfo(){
        $db = new Sql($this->adapter['read']);

        $select = $db->select()
        ->from('system_config')
        ->limit(1);
        
        // echo $db->buildSqlString($select); exit();

        $stmt = $db->prepareStatementForSqlObject($select);
        $results = $stmt->execute();
        $data = $results->current();
        return $data;
    }

    public function changeTimeZone($dateString, $timeZoneSource = null, $timeZoneTarget = null)
    {
        if (empty($timeZoneSource)) {
            $timeZoneSource = date_default_timezone_get();
        }
        if (empty($timeZoneTarget)) {
            $timeZoneTarget = date_default_timezone_get();
        }

        $dt = new \DateTime($dateString, new \DateTimeZone($timeZoneSource));
        $dt->setTimezone(new \DateTimeZone($timeZoneTarget));

        return $dt->format("Y-m-d H:i:s");
    }
}
?>